from django.shortcuts import render
from rest_framework import generics , status
from .serializers import Informacionserializer
from .serializers import DatosAPIserializer
from .models import Informacion
from .models import DatosAPI
from rest_framework.views import APIView
from rest_framework.response import Response

import requests as rq
import json

headers = {
    'x-rapidapi-key': "c55f4f43f8msh443f166cb1daea3p12e125jsn5a7b28149a80",
    'x-rapidapi-host': "aylien-text.p.rapidapi.com"
    }


def informacion_data(headers, texto):
    url = "https://aylien-text.p.rapidapi.com/entities"
    querystring = {"text": texto}
    response = rq.request("GET", url, headers=headers, params=querystring)
    resultado = json.loads(response.text)
    lenguaje = str(resultado['language'])
    entidades = str(resultado['entities'])

    url = "https://aylien-text.p.rapidapi.com/sentiment"
    response = rq.request("GET", url, headers=headers, params=querystring)
    resultado = json.loads(response.text)
    polaridad = str(resultado['polarity'])
    coef_pol = str(resultado['polarity_confidence'])
    coef_subj = str(resultado["subjectivity_confidence"])

    return lenguaje, polaridad, coef_pol, coef_subj , entidades






class InformacionView(APIView):
    queryset = Informacion.objects.all()
    serializer_class = Informacionserializer

    def post(self,request, format=None):
        self.request.session.create()
        serializer = self.serializer_class(data= request.data)
        if serializer.is_valid():
            usuario = serializer.data.get('usuario')
            texto = serializer.data.get('texto')
            informacion = Informacion(usuario=usuario, texto= texto)
            informacion.save()
        
        return Response(Informacionserializer(informacion).data, status=status.HTTP_201_CREATED)



class DatosAPIView(generics.ListCreateAPIView):
    queryset = DatosAPI.objects.all()
    serializer_class = DatosAPIserializer

    def post(self, request, format=None):
        self.request.session.create()
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            texto = serializer.data.get('texto')
            idioma, polaridad, coeficiente_polaridad, coeficiente_subjetividad, entidades = informacion_data(headers,texto)
        datos= DatosAPI(texto=texto,
                        idioma=idioma,
                        polaridad=polaridad, 
                        coeficiente_polaridad=coeficiente_polaridad, 
                        coeficiente_subjetividad=coeficiente_subjetividad, 
                        entidades= entidades)
        datos.save()

        return Response(DatosAPIserializer(datos).data , status= status.HTTP_200_OK)


class GetDatosAPI(APIView):
    serializer_class = DatosAPIserializer
    look_texto = 'texto'
    def get(self,request,format=None):
        texto = request.GET.get(self.look_texto)
        if texto != None:
            datos = DatosAPI.objects.filter(texto=texto)
            data= DatosAPIserializer(datos[0]).data
            return Response(data, status=status.HTTP_200_OK)
        return Response({'Texto no escrito': 'pruebe de nuevo'}, status= status.HTTP_400_BAD_REQUEST)
            


        










