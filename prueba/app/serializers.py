from rest_framework import serializers
from .models import Informacion
from .models import DatosAPI

class Informacionserializer(serializers.ModelSerializer):
    class Meta:
        model = Informacion
        fields = ("id", "usuario", "texto", "fecha")

class DatosAPIserializer(serializers.ModelSerializer):
    class Meta:
        model = DatosAPI
        fields = ("id", "texto", "idioma", "polaridad","coeficiente_polaridad","coeficiente_subjetividad","entidades")
        

