from django.urls import path
from .views import InformacionView
from .views import DatosAPIView , GetDatosAPI

urlpatterns = [
    path('home', InformacionView.as_view()),
    path('data', DatosAPIView.as_view()),
    path('gets', GetDatosAPI.as_view())
]