from django.db import models



class Informacion(models.Model):
    usuario = models.CharField(max_length=8000, default = "Anonimo")
    texto = models.CharField(max_length=70000, default="")
    fecha= models.DateField(auto_now_add=True)

class DatosAPI(models.Model):
    texto = models.CharField(max_length=70000, default="")
    idioma = models.CharField(max_length=8000, default ="" )
    polaridad = models.CharField(max_length=8000, default = "")
    coeficiente_polaridad =  models.CharField(max_length=8000, default = "")
    coeficiente_subjetividad=  models.CharField(max_length=8000, default = "")
    entidades = models.CharField(max_length=80000 , default="")






