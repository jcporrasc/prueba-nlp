# Generated by Django 3.2 on 2021-04-12 03:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_alter_datosapi_idioma'),
    ]

    operations = [
        migrations.AlterField(
            model_name='datosapi',
            name='idioma',
            field=models.CharField(max_length=8000),
        ),
    ]
